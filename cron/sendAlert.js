const { giphyService, slackService } = require('../services');

module.exports = async () => {
  try {
    const lunchImage = await giphyService.getFoodImage();
    slackService.sendPlainMessage(lunchImage);
  }
  catch (error) {
    slackService.sendSuccessMessage('Lunch time :exclamation:', ':knife_fork_plate:');
  }
};