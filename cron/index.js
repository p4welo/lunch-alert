const cron = require('node-cron');
const sendAlert = require('./sendAlert');

const init = () => {
  cron.schedule('0 12 * * 1-5', () => {
    sendAlert();
  });
};

module.exports = {
  init
};