const cron = require('./cron');

const express = require('express');
const app = express();

app.get('/', (req, res) => res.json({ status: 'OK' }));
app.listen(3000, () => {
  cron.init();
  console.log('app start');
});