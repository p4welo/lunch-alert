const fetch = require('node-fetch');
const config = require('../config/app');

const getFoodImage = async () => {
  return await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${config.GIPHY_KEY}&tag=food`)
      .then(res => res.json())
      .then(response => response.data.fixed_height_downsampled_url)
};

module.exports = {
  getFoodImage
};