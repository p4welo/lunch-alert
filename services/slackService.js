const config = require('../config/app');
const { IncomingWebhook } = require('@slack/client');
const webhook = new IncomingWebhook(config.SLACK_WEBHOOK);

const sendPlainMessage = (text) => {
  console.log('sendPlainMessage');
  webhook.send({ text });
};

const sendSuccessMessage = (title, text) => {
  console.log('sendSuccessMessage');
  webhook.send({
    attachments: [
      { color: 'good', title, text }
    ]
  })
};

module.exports = {
  sendPlainMessage,
  sendSuccessMessage
};